{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance ToJSON Person where
    toJSON p =
        object ["first" .= firsname p,
                "last"  .= lasname p,
                "birth" .= show (birthyear p)
                ]

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
    , Person "Haskell" "Curry" 1900
    ]

main :: IO()
main = encodeFile "out-aeson1.json" persons
