{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.Lazy as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

data Address = Address 
    {
        number :: Int, 
        road :: T.Text, 
        zipcode :: Int, 
        city :: T.Text 
    } deriving (Generic, Show)


instance ToJSON Person where
    toJSON p =
        object ["first" .= firsname p,
                "last"  .= lasname p,
                "birth" .= show (birthyear p)
                ]

instance ToJSON Address

persons :: [Person]
persons =
    [ Person "John" "Doe" 1970
        (Address 42 "Pont Vieux" 43000 "Espaly")
    , Person "Haskell" "Curry" 1900
        (Address 1337 "Père Lachaise" 75000 "Paris")
    ]



main :: IO()
main = encodeFile "out-aeson1.json" persons

