{-# LANGUAGE DeriveGeneric #-}
import Data.Aeson
import GHC.Generics
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person

main :: IO ()
main = do
    res0 <- eitherDecodeFileStrict "aeson-test1.json"
    print (res0 :: Either String Person)

    res1 <- eitherDecodeFileStrict "aeson-test2.json"
    print (res1 :: Either String [Person])

    res2 <- eitherDecodeFileStrict "aeson-test3.json"
    print (res2 :: Either String [Person])

