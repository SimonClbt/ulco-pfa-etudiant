import qualified Data.Text.IO as T
import qualified Data.ByteString.Char8 as B
import Data.Text.Encoding (encodeUtf8)

main :: IO()
main = do
    content <- T.readFile "text4.hs"
    B.putStrLn $ encodeUtf8 content