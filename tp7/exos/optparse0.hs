import Options.Applicative

data Args = Args
    { hello      :: String
    , val1       :: Int
    } deriving (Show)

argsP :: Parser Args
argsP = Args
      <$> strArgument(metavar "<fichuier de sortie>")
      <*> strOption
          ( long "hello"
         <> help "Target for the greeting"
         <> metavar "TARGET")
      <*> option auto
          ( long "val1"
         <> help "Value 1"
         <> metavar "INT"
         <> value 1)
      <*> option auto
          ( long "val2"
         <> help "Value "
         <> metavar "DOUBLE"
         <> value 2)

myinfo :: ParserInfo Args
myinfo = info (argsP <**> helper)
              (fullDesc <> header "This is my cool app!")

main :: IO ()
main = execParser myinfo >>= print

