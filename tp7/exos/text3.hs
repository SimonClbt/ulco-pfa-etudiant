import qualified Data.Text.IO as T
import qualified Data.ByteString.Char8 as B
import Data.Text.Encoding (decodeUtf8)

main :: IO()
main = do
    content <- B.readFile "text3.hs"
    T.putStrLn $ decodeUtf8 content