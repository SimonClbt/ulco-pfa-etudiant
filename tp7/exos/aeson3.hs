{-# LANGUAGE OverloadedStrings #-}

import Data.Aeson
import qualified Data.Text as T

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: Int
    } deriving (Show)

instance FromJSON Person where
        parseJSON = withObjetc "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: (read <$> v .: "birthyear")

main :: IO ()
main = do


    res0 <- eitherDecodeFileStrict "aeson-test1.json"
    print (res0 :: Either String Person)

