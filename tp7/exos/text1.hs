import qualified Data.ByteString.Char8 as B

main :: IO()

main = do
    content <- B.readFile "./text1.hs"
    putStrLn $ B.unpack content
