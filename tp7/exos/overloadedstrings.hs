{-# LANGUAGE OverloadedStrings #-}
import Data.Text

newtype Person = Person Text deriving Show

persons :: [Person]
persons = [Person "John", Person "Haskell"]

main :: IO ()
main = print persons

