
mymap1 :: (a -> b) -> [a] -> [b]
mymap1 _ [] = []
mymap1 f (x:xs) = f x : mymap1 f xs 

-- mymap2

myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = []
myfilter1 p (x:xs) = if p x then x : myfilter1 p xs else myfilter1 p xs

-- myfilter2 

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ mymap1 (*2) [1,2]
    print $ myfilter1 even []

