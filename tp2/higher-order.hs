import Data.List

threshList :: Ord a => a -> [a] -> [a]
threshList s xs = map (min s) xs

selectList :: Ord a => a -> [a] -> [a]
selectList s = filter (<s) 

maxList :: Ord a => [a] -> a
maxList xs = foldr1 max xs 

main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]
    print $ selectList 3 [1..5::Int]
    print $ maxList [13, 42, 37::Int]

