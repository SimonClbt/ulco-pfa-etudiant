import Data.Char

isSorted :: Ord a => [a] -> Bool
isSorted [] = True
isSorted [_] = True
isSorted (x0:x1:xs) = x0 < x1 && isSorted(x1:xs)

nocaseCmp :: String -> String -> Bool
nocaseCmp xs ys = (map toUpper xs) < (map toUpper ys)

main :: IO ()
main = do
    print $ isSorted [1..4]
    print $ isSorted [4,3..1]

