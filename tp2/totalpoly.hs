
safeTailString :: String -> String
safeTailString ""     = ""
safeTailString (x:xs) = xs

safeHeadString :: String -> Maybe Char
safeHeadString "" = Nothing
safeHeadString (x:xs) = Just x

safeTail :: [a] -> [a]
safeTail [] = []
safeTail (x:xs) = xs

safeHead :: [a] -> Maybe a
safeHead [] = Nothing 
safeHead (x:xs) = Just x

main :: IO ()
main = putStrLn "TODO"

