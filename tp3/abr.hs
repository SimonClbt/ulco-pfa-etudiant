import Data.List (foldl')

data Abr = Empty | Node Int Abr Abr
    deriving (Show)

insererAbr :: Int -> Abr -> Abr
insererAbr x Empty = Node x Empty Empty
insererAbr x (Node y l r) = if x < y then Node y (insererAbr x l) r else Node y l (insererAbr x r)

listToAbr :: [Int] -> Abr
listToAbr [] = Empty
listToAbr (h:t) = insererAbr h (listToAbr t)

abrToList :: Abr -> [Int]
abrToList Empty = []
abrToList (Node x l r) = abrToList l ++ [x] ++ abrToList r

main :: IO ()
main = do
    print $ listToAbr [42, 13]
    print $ abrToList $ listToAbr [42, 13, 37]

