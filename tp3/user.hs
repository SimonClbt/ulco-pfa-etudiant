
data User = User {
    _nom :: String, _prenom :: String,  _age :: Int }
    

showUser :: User -> String

showUser user = _nom user ++ " " ++ _prenom user ++ " " ++ show (_age user)


incAge :: User -> User
incAge u = u {_age = _age u + 1}

main :: IO ()
main = do
    let u1 = User "Simon" "Clerbout" 23
    print $ showUser (u1)

