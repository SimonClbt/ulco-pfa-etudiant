
data List a = Empty | Cons a (List a)

sumList :: Num a => List a -> a
sumList (Cons h t) = h + sumList t   

-- flatList 

concatList :: List a -> List a -> List a 
concatList (Cons x xs) ys =  Cons x (concatList xs ys)

toHaskell :: List a -> [a]
toHaskell (Cons h t) = h : toHaskell t

fromHaskell :: [a] -> List a
fromHaskell (h:t) = Cons h (fromHaskell t)

myShowList :: Show a => List a -> String
myShowList (Cons h t) = show h ++ "" ++ myShowList t

main :: IO ()
main = putStrLn "TODO"

