import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

main :: IO ()
main = do
    forM_ [1..4] $ \i -> do
        putStr $ "saisie " ++ show i ++ " : "
        hFlush stdout
        res <- getLine
        let mx = readMaybe res :: Maybe Int
        case mx of
            Nothing -> putStrLn "-> saisie invalide"
            Just x -> putStrLn $ "-> vous avez saisi l'entier : " ++ show x
    

