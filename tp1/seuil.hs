-- seuilInt
seuilInt :: Int -> Int -> Int -> Int
seuilInt x0 x1 x
    | x < x0 = x0
    | x > x1 = x1
    | otherwise = x

-- seuilTuple
seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (x0,x1) x = max x0 $ min x1 x 

main :: IO ()
main = do
    print $ seuilInt 1 10 0
    print $ seuilInt 1 10 2
    print $ seuilInt 1 10 42
    print $ seuilTuple (1, 10) 0
    print $ seuilTuple (1, 10) 2
    print $ seuilTuple (1, 10) 42

