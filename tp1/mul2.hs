mul2 :: Int -> Int
mul2 x = 2 * x

main :: IO ()
main = do
    print $ mul2 42
    print $ mul2 (-42)
    

